<?php

namespace App\Controller;

use App\Form\AddImageFormType;
use App\Services\VigenereService;
use GdImage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     *
     * @return Response
     */
    public function index(VigenereService $vigenereService, KernelInterface $kernelInterface, Request $request): Response
    {
        $form = $this->createForm(AddImageFormType::class);

        $form->handleRequest($request);

        if($form->isSubmitted()){
            /**
             * @var string
             */
            $message = $form->getData()["message"];

            $key = $form->getData()["key"];

            $message = $vigenereService->encrypt($key, $message);


            $octect_decoupe = [];

            $f_image = fopen($kernelInterface->getProjectDir() . "/public/sample_1920×1280.bmp", 'r+b');
            // $s_image = fopen($kernelInterface->getProjectDir() . "/public/tissus3.bmp", 'r+b');

            // $message = $vigenereService->encrypt("cyberkey",stream_get_contents($s_image));
            // dd($f_image);
            fseek($f_image, 54);

            $message .= chr(26);

           
            for($i = 0; $i < strlen($message); $i++){
                $caractere = $message[$i];

                $valeur_octet = ord($caractere);

                $octect_binaire = decbin($valeur_octet);

                $octect_binaire = str_pad($octect_binaire, 8, '0', STR_PAD_LEFT);

                $octect_decoupe = str_split($octect_binaire, 2);

                foreach ($octect_decoupe as $partie_octet) {
                    $octet_image = fread($f_image, 1);
                    $octet_image = ord($octet_image);
                    $octet_image -= $octet_image%4;
                    $partie_octet = bindec($partie_octet);
                    $octet_image += $partie_octet;
                    fseek($f_image, -1, SEEK_CUR);
                    fputs($f_image, chr($octet_image));
                }
            }

            fclose($f_image);
        }

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/steno", name="steno")
     *
     * @return Response
     */
    public function testSteno(VigenereService $vigenereService, KernelInterface $kernelInterface, Request $request)
    {
        /**
         * @var string
         */
        $message = "";

        $tampon = "";

        // $s_image = fopen($kernelInterface->getProjectDir() . "/public/tissus3.bmp", 'r+b');

        //     $simage = stream_get_contents($s_image);

        // dump($simage);
    

        $f_image = fopen($kernelInterface->getProjectDir() . "/public/sample_1920×1280.bmp", 'r+b');
        // dd($f_image);
        fseek($f_image, 54);

        while(!feof($f_image)){
            $octet_image = fread($f_image, 1);

            $octet_image = ord($octet_image);

            $bits_pf = $octet_image%4;
            $bits_pf = decbin($bits_pf);
            $bits_pf = str_pad($bits_pf, 2, '0', STR_PAD_LEFT);

            $tampon .= $bits_pf;

            if(strlen($tampon) == 8) {
                $tampon = bindec($tampon);
                
                if(intval($tampon) === 26) {

                    dump("**********");
                    dump("message codé : ", $message);
                    dump("**********");
                    $message = $vigenereService->decrypt("cyberkey",$message);

                    // file_put_contents($kernelInterface->getProjectDir() . "/private/image.bmp", base64_decode($message));
                    dump("**********");
                    dd("message décodé : ", $message);
                    
                    return new JsonResponse(json_encode($message)   );
                }

                $message .= chr($tampon);
                // dd($message);
                $tampon = "";
            }
        }
    }
}
